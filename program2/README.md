﻿## Jake Petek -- Program 1

# Controls
* The zoom is controlled with the mouse wheel. Scrolling up zooms in and scrolling down zooms out.
* The view can be rotated using a virtual track ball. This can be done by clicking and dragging in the view.
* All of the other features can be changed using the sliders on the right side of the screen.

# Features Explanation
* Brick Width: slider that changes the width of the brick with a range of [.2,1].
* Brick Height: slider that changes the height of the brick with a range of [.2,1].
* Brick Depth: slider that changes the depth of the brick with a range of [.2,1].
* Brick Spacing: slider that changes the distance between the bricks (the mortar) with a range of [.05,.15].
* Wall Width: slider that changes the width of the wall in bricks with a range of [1,20].
* Wall Height: slider that changes the height of the wall in bricks with a range of [1,20].
* Light Color: there are sliders for Red, Green, and Blue components of the light each with a range of [0,1].
* Light Intensity: slider that changes the light intensity with a range of [0,5].
* Normal Wall: radio button which constructs a single wall.
* Square Wall: radio button which constructs four walls which form a square.
* Brick Color: The brick color is slightly randomized in order to guve the wall the look of a real brick wall.