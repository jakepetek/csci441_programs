#version 330
uniform mat4 view;

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightIntensity;

in vec3 fcolor;
in vec3 fposition;
in vec3 fnormal;
out vec4 color_out;

void main() {
    vec3 L;
    vec3 N = normalize(fnormal);
    vec3 V = normalize((inverse(view))[3].xyz);
    int shine = 10;

    L = normalize(lightPos-fposition);
    vec3 R = dot(L,N)*N;
    float s = max(0, dot(V,R));
    s = pow(s,shine);
    vec3 specular = fcolor*s;

    vec3 diffuse = fcolor*max(dot(N,L),0);

    color_out = vec4(lightIntensity*lightColor*(diffuse+specular)+.3*fcolor, 1);
  //color_out = vec4(fcolor, 1);
}
