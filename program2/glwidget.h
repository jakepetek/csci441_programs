#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>

#define GLM_FORCE_RADIANS

using glm::mat4;
using glm::vec3;
using namespace std;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT
    public slots:
        void widthOfBrick(int w);
        void heightOfBrick(int h);
        void depthOfBrick(int d);
        void widthOfWall(int w);
        void heightOfWall(int h);
        void spaceBetweenBricks(int s);
        void makeWallButton();
        void makeSquareWallButton();
        void lightColR(int r);
        void lightColG(int g);
        void lightColB(int b);
        void lightIntensity(int l);
    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void wheelEvent(QWheelEvent *event);
        void keyPressEvent(QKeyEvent *event);

    private:
        void renderWall();

        GLuint wallProg;
        GLuint wallVao;
        GLint wallProjMatrixLoc;
        GLint wallViewMatrixLoc;
        GLint wallModelMatrixLoc;
        GLint wallRotateMatrixLoc;

        GLint wallLightPosLoc;
        GLint wallLightColorLoc;
        GLint wallLightIntensityLoc;

        float viewDist;

        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;
        GLint gridRotateMatrixLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;
        mat4 rotateMatrix;

        int width;
        int height;
        vec3 prevPoint;
        float brickWidth;
        float brickHeight;
        float brickDepth;
        float brickSpacing;
        int bricksWide;
        int bricksTall;

        int lx;
        int ly;
        int lz;
        float lr;
        float lg;
        float lb;
        float li;

        GLuint positionBuffer;
        GLuint colorBuffer;
        GLuint indexBuffer;
        GLuint normalBuffer;

        vector<vec3> pts;
        vector<vec3> colors;
        vector<int> indices;
        vector<vec3> normals;

        bool normalWall;

        void initializeWall();
        void makeWall();
        void makeSquareWall();
        void makeNormalWall();
        void makeBrick(float x1, float x2, float y1, float y2, float z1, float z2, vec3 color, int indice);
        void updateLight();

        glm::vec3 pointOnVirtualTrackball(const glm::vec2 &pt);
};

#endif
