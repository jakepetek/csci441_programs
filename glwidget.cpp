#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::vec2;
using glm::vec3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;

using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) { 
}

GLWidget::~GLWidget() {
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {

    }
}

void GLWidget::wheelEvent(QWheelEvent *event) {
    makeCurrent();
    viewDist += event->delta()/25;

    if (viewDist > -5.0f) {
        viewDist = -5.0f;
    }

    viewMatrix = lookAt(vec3(0,0,viewDist),vec3(0,0,0),vec3(0,1,0));

    glUseProgram(wallProg);
    glUniformMatrix4fv(wallViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    update();
}

void GLWidget::makeBrick(float x1, float x2, float y1, float y2, float z1, float z2, vec3 color, int indice){
    GLuint restart = 0xFFFFFFFF;
    vec3 brick[] = {
        // top
        vec3(x2,y2,z2),    // 0
        vec3(x2,y2,z1),   // 1
        vec3(x1,y2,z1),  // 2
        vec3(x1,y2,z2),   // 3

        // bottom
        vec3(x2,y1,z2),   // 4
        vec3(x1,y1,z2),  // 5
        vec3(x1,y1,z1), // 6
        vec3(x2,y1,z1),  // 7

        // front
        vec3(x2,y2,z1),    // 8
        vec3(x1,y2,z1),   // 9
        vec3(x1,y1,z1),  // 10
        vec3(x2,y1,z1),   // 11

        // back
        vec3(x2,y2,z2), // 12
        vec3(x1,y2,z2),  // 13
        vec3(x1,y1,z2),   // 14
        vec3(x2,y1,z2),  // 15

        // right
        vec3(x2,y1,z2),   // 16
        vec3(x2,y1,z1),  // 17
        vec3(x2,y2,z1),   // 18
        vec3(x2,y2,z2),     // 19

        // left
        vec3(x1,y1,z2),  // 20
        vec3(x1,y2,z2),   // 21
        vec3(x1,y2,z1),  // 22
        vec3(x1,y1,z1) // 23
    };
    for(int k = 0; k < 24; k++){
        pts.push_back(brick[k]);
        colors.push_back(color);
        indices.push_back(indice+k);
        if((indice+k+1) % 4 == 0){
            indices.push_back(restart);
        }
    }
}

void GLWidget::makeWall(){
    pts.clear();
    colors.clear();
    indices.clear();
    int indice = 0;
    float minX = -1*((bricksWide * brickWidth + (bricksWide-1) * brickSpacing)/2);
    float maxX = (bricksWide * brickWidth + (bricksWide-1) * brickSpacing)/2;
    float x = minX;
    float y = 0;
    float offset = brickWidth/2;
    for(int i = 0; i < bricksTall; i++){
        for(float j = 0; j < bricksWide; j++){
            float r1 = ((float) rand()) / (float) RAND_MAX*0.1f;
            float r2 = ((float) rand()) / (float) RAND_MAX*0.1f;
            float r3 = ((float) rand()) / (float) RAND_MAX*0.1f;
            //cout << r1 << " " << r2 << " " <<r3 << endl;
            vec3 col = vec3(.45+r1, .07+r2, .10 + r3);
            if((i+1) % 2 == 0){
                if(j == 0){
                    makeBrick(x,x+offset, y, y+brickHeight, 0, brickDepth, col, indice);
                }
                else{
                    makeBrick(x-offset,x+brickWidth-offset, y, y+brickHeight, 0, brickDepth, col, indice);
                }
                indice += 24;
                if(j < bricksWide){
                    makeBrick(x+brickWidth-offset,x+brickWidth+brickSpacing-offset, y, y+brickHeight, .01f, brickDepth-.01, vec3(0.5,0.5,0.5), indice);
                    indice += 24;
                }
                if(j == bricksWide-1){
                    makeBrick(x+brickWidth+brickSpacing-offset,x+brickWidth, y, y+brickHeight, 0, brickDepth, col, indice);
                    indice += 24;
                }
            }
            else{
                makeBrick(x,x+brickWidth, y, y+brickHeight, 0, brickDepth, col, indice);
                indice += 24;
                if(j < bricksWide-1){
                    makeBrick(x+brickWidth,x+brickWidth+brickSpacing, y, y+brickHeight, .01f, brickDepth-.01, vec3(0.5,0.5,0.5), indice);
                    indice += 24;
                }
            }
            x += (brickWidth+brickSpacing);
        }
        if(i < bricksTall - 1){
            makeBrick(minX, maxX, y+brickHeight, y+brickHeight+brickSpacing, .01f, brickDepth-.01, vec3(0.5,0.5,0.5), indice);
            indice += 24;
        }
        x = minX;
        y += (brickHeight+brickSpacing);
    }

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, pts.size()*sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, colors.size()*sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(int), &indices[0], GL_DYNAMIC_DRAW);
    update();
}

void GLWidget::initializeWall(){
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &wallVao);
    glBindVertexArray(wallVao);

    // Create a buffer on the GPU for position data
    glGenBuffers(1, &positionBuffer);
    glGenBuffers(1, &colorBuffer);
    glGenBuffers(1, &indexBuffer);

    makeWall();
    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    wallProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    wallProjMatrixLoc = glGetUniformLocation(program, "projection");
    wallViewMatrixLoc = glGetUniformLocation(program, "view");
    wallModelMatrixLoc = glGetUniformLocation(program, "model");
    wallRotateMatrixLoc = glGetUniformLocation(program, "rotate");
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    prevPoint = vec3(0,0,0);
    brickHeight = .3f;
    brickWidth = .7f;
    brickDepth = .4f;
    brickSpacing = .07f;
    bricksWide = 8;
    bricksTall = 8;
    viewDist = -10;

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);

    initializeWall();
    rotateMatrix = mat4(0.5f);
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, 1.0f, 100.0f);
    viewMatrix = lookAt(vec3(0,0,viewDist),vec3(0,0,0),vec3(0,1,0));
    modelMatrix = mat4(1.0f);


    glUseProgram(wallProg);
    glUniformMatrix4fv(wallProjMatrixLoc, 1, false, value_ptr(projMatrix));
    glUniformMatrix4fv(wallViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(wallModelMatrixLoc, 1, false, value_ptr(modelMatrix));
    glUniformMatrix4fv(wallRotateMatrixLoc, 1, false, value_ptr(rotateMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderWall();
}

void GLWidget::renderWall() {
    glUseProgram(wallProg);
    glBindVertexArray(wallVao);
    glDrawElements(GL_TRIANGLE_FAN, indices.size(), GL_UNSIGNED_INT, 0);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);

    return program;
}

// Part 2 - As we click and drag, use the pointOnVirtualTrackball
// to construct an angle and axis which we can use to create a
// rotation matrix with using glm::rotate.
// Keep track of the last mouse click or movement along with the
// current one to perform the above calulation. Continuously
// multiply the matrices to accumulate a single trackball rotation
// matrix.
void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec3 ballPt = pointOnVirtualTrackball(pt);

    prevPoint = ballPt;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec3 ballPt = pointOnVirtualTrackball(pt);
    float angle = acos(dot(normalize(ballPt),normalize(prevPoint)));
    vec3 axis = cross(normalize(ballPt),normalize(prevPoint));
    if(length(axis) > 0.00001f){
        axis = normalize(axis);
        rotateMatrix = rotate(mat4(1.0),angle,axis)*rotateMatrix;
        prevPoint = ballPt;
        glUseProgram(wallProg);
        glUniformMatrix4fv(wallRotateMatrixLoc, 1, false, value_ptr(rotateMatrix));
        update();
    }
}

vec3 GLWidget::pointOnVirtualTrackball(const vec2 &pt) {
    // Part 1 - implement the equations for a virtual trackball to
    // convert the provided pt parameter to a 3D point on the virtual
    // trackball surface.
    vec3 point;
    float radius = .5f;

    point.x = pt.x * (2.0f/(float)width) - 1;
    point.y = (float)height/(float)width * (pt.y * (2.0f/(float)height) - 1);

    if(point.x*point.x + point.y*point.y <= 0.5f*radius*radius){
        point.z = sqrt(radius*radius - (point.x*point.x + point.y*point.y));
    }
    else{
        point.z = 0.5f*radius*radius/sqrt(point.x*point.x + point.y*point.y);
    }

    return point;
}

void GLWidget::widthOfBrick(int w){
    brickWidth = (float)w/100;
    cout << "The width of the bricks is now " << brickWidth <<"." << endl;
    makeWall();
}

void GLWidget::heightOfBrick(int h){
    brickHeight = (float)h/100;
    cout << "The height of the bricks is now " << brickHeight <<"." << endl;
    makeWall();
}

void GLWidget::depthOfBrick(int d){
    brickDepth = (float)d/100;
    cout << "The depth of the bricks is now " << brickDepth <<"." << endl;
    makeWall();
}

void GLWidget::spaceBetweenBricks(int s){
    brickSpacing = (float)s/100;
    cout << "The space between bricks is now " << brickSpacing <<"." << endl;
    makeWall();
}

void GLWidget::widthOfWall(int w){
    bricksWide = w;
    cout << "The wall is now " << bricksWide <<" bricks wide." << endl;
    makeWall();
}

void GLWidget::heightOfWall(int h){
    bricksTall = h;
    cout << "The wall is now " << bricksTall <<" bricks tall." << endl;
    makeWall();
}
