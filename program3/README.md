﻿## Jake Petek -- Program 1

# Controls
* Move with W,A,S, and D
* W: move forward
* A: move left
* S: move back
* D: move right
* The view can be rotated using a virtual track ball. This can be done by clicking and dragging in the view.

# Explanation
* Enter the maze, the maze is constructed using the walls from program 2.
* There is a wall in the maze which moves, revealing the path forward.
* At the end of the maze is the golden rabbit room.