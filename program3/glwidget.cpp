#include "glwidget.h"
#include <iostream>
#include <QOpenGLTexture>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using glm::inverse;
using glm::vec2;
using glm::vec3;
using glm::mat3;
using glm::mat4;
using glm::perspective;
using glm::normalize;
using glm::length;
using glm::cross;
using glm::dot;
using glm::rotate;
using glm::value_ptr;
using glm::lookAt;
using namespace std;

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent) { 
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(animate()));
    timer->start(16);
    forward = false;
    back = false;
    right = false;
    left = false;
    up = false;
    down = false;
    fly = false;
}

GLWidget::~GLWidget() {
}

void GLWidget::animate(){
    float dt = 0.016f;
    float speed = 3.0f;
    vec3 upVec = vec3(0,1,0);
    velocity = vec3(0,0,0);
    if(!fly)
        velocity = -upVec;

    vec3 forwardVec = -vec3(yawMatrix[2]);
    if(fly)
        forwardVec = -vec3((pitchMatrix*yawMatrix)[2]);
    vec3 rightVec = vec3(yawMatrix[0]);
    if(forward)
        velocity += forwardVec;
    if(back)
        velocity -= forwardVec;
    if(right)
        velocity += rightVec;
    if(left)
        velocity -= rightVec;

    if(up)
        if(!fly)
            velocity += 2.5f * upVec;
        else
            velocity += upVec;

    if(down)
        velocity -= upVec;

    if(length(velocity) > 0)
        position += (normalize(velocity)* speed * dt);

    if(position.y < 0)
        position.y = 0;

    float ct = 0.005f;
    time += ct;

    if(time >= 5 && time < 6){
        for(int i = moveWallStart; i < pts.size(); i++){
            pts[i] += vec3(.015,0,0);
        }
    }
    else if(time >= 9 && time < 10){
        for(int i = moveWallStart; i < pts.size(); i++){
            pts[i] -= vec3(.015,0,0);
        }
    }
    else if(time >= 10){
        time = 0;
    }

    viewMatrix = inverse(glm::translate(mat4(1.0f), position)*(pitchMatrix * yawMatrix));
    glUseProgram(wallProg);
    glUniformMatrix4fv(wallViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, pts.size()*sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);
    glUniformMatrix4fv(wallModelMatrixLoc, 1, false, value_ptr(modelMatrix));
    glUseProgram(modelProg);
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    //std::cout << velocity.x << " " << velocity.y << " " << velocity.z << std::endl;
    update();
}

void GLWidget::initializeGrid() {
    glGenVertexArrays(1, &gridVao);
    glBindVertexArray(gridVao);

    // Create a buffer on the GPU for position data
    GLuint positionBuffer;
    glGenBuffers(1, &positionBuffer);

    //vec3 pts[84];
    /*for(int i = -10; i <= 10; i++) {

        pts[2*(i+10)] = vec3(i, -.5f, 10);
        pts[2*(i+10)+1] = vec3(i, -.5f, -10);

        pts[2*(i+10)+42] = vec3(10,-.5f, i);
        pts[2*(i+10)+43] = vec3(-10,-.5f, i);
    }*/

    vec3 pts[4];
    pts[0] = vec3(-9, -.5f, -15);
    pts[1] = vec3(9, -.5f, -15);
    pts[2] = vec3(9, -.5f, 6);
    pts[3] = vec3(-9, -.5f, 6);

    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pts), pts, GL_STATIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/grid_vert.glsl", ":/grid_frag.glsl");
    glUseProgram(program);
    gridProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information 
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    gridProjMatrixLoc = glGetUniformLocation(program, "projection");
    gridViewMatrixLoc = glGetUniformLocation(program, "view");
    gridModelMatrixLoc = glGetUniformLocation(program, "model");

    gridDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    gridAmbientColorLoc = glGetUniformLocation(program, "ambientColor");
    gridSpecularColorLoc = glGetUniformLocation(program, "specularColor");

    glUniform3f(gridAmbientColorLoc, .2,.2,.23);
    glUniform3f(gridDiffuseColorLoc, .2,.2,.23);
    glUniform3f(gridSpecularColorLoc, .2,.2,.23);

    gridLightPosLoc = glGetUniformLocation(program, "lightPos");
    gridLightColorLoc = glGetUniformLocation(program, "lightColor");
    gridLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    glUniform3f(gridLightPosLoc, 5,10,-2);
    glUniform3f(gridLightColorLoc, 1,1,1);
    glUniform1f(gridLightIntensityLoc, 1);
}

void GLWidget::makeBrick(float x1, float x2, float y1, float y2, float z1, float z2, vec3 color, int indice){
    GLuint restart = 0xFFFFFFFF;
    vec3 brick[] = {
        // top
        vec3(x2,y2,z2),    // 0
        vec3(x2,y2,z1),   // 1
        vec3(x1,y2,z1),  // 2
        vec3(x1,y2,z2),   // 3

        // bottom
        vec3(x2,y1,z2),   // 4
        vec3(x1,y1,z2),  // 5
        vec3(x1,y1,z1), // 6
        vec3(x2,y1,z1),  // 7

        // front
        vec3(x2,y2,z1),    // 8
        vec3(x1,y2,z1),   // 9
        vec3(x1,y1,z1),  // 10
        vec3(x2,y1,z1),   // 11

        // back
        vec3(x2,y2,z2), // 12
        vec3(x1,y2,z2),  // 13
        vec3(x1,y1,z2),   // 14
        vec3(x2,y1,z2),  // 15

        // right
        vec3(x2,y1,z2),   // 16
        vec3(x2,y1,z1),  // 17
        vec3(x2,y2,z1),   // 18
        vec3(x2,y2,z2),     // 19

        // left
        vec3(x1,y1,z2),  // 20
        vec3(x1,y2,z2),   // 21
        vec3(x1,y2,z1),  // 22
        vec3(x1,y1,z1) // 23
    };
    vec3 norm[] = {
        // top
        vec3(0,1,0),    // 0
        vec3(0,1,0),   // 1
        vec3(0,1,0),  // 2
        vec3(0,1,0),   // 3

        // bottom
        vec3(0,-1,0),   // 4
        vec3(0,-1,0),  // 5
        vec3(0,-1,0), // 6
        vec3(0,-1,0),  // 7

        // front
        vec3(0,0,-1),    // 8
        vec3(0,0,-1),   // 9
        vec3(0,0,-1),  // 10
        vec3(0,0,-1),   // 11

        // back
        vec3(0,0,1), // 12
        vec3(0,0,1),  // 13
        vec3(0,0,1),   // 14
        vec3(0,0,1),  // 15

        // right
        vec3(1,0,0),   // 16
        vec3(1,0,0),  // 17
        vec3(1,0,0),   // 18
        vec3(1,0,0),     // 19

        // left
        vec3(-1,0,0),  // 20
        vec3(-1,0,0),   // 21
        vec3(-1,0,0),  // 22
        vec3(-1,0,0) // 23
    };
    for(int k = 0; k < 24; k++){
        pts.push_back(brick[k]);
        colors.push_back(color);
        indices.push_back(indice+k);
        normals.push_back(norm[k]);
        if((indice+k+1) % 4 == 0){
            indices.push_back(restart);
        }
    }
}

void GLWidget::makeWall(int bricksWide, int bricksTall, float brickWidth, float brickHeight, float brickDepth, float brickSpacing){
    //pts.clear();
    //colors.clear();
    //indices.clear();
    //normals.clear();
    int indice = pts.size();
    float minX = -1*((bricksWide * brickWidth + (bricksWide-1) * brickSpacing)/2);
    float maxX = (bricksWide * brickWidth + (bricksWide-1) * brickSpacing)/2;
    float x = minX;
    float y = 0;
    float offset = brickWidth/2;
    for(int i = 0; i < bricksTall; i++){
        for(float j = 0; j < bricksWide; j++){
            float r1 = ((float) rand()) / (float) RAND_MAX*0.1f;
            float r2 = ((float) rand()) / (float) RAND_MAX*0.1f;
            float r3 = ((float) rand()) / (float) RAND_MAX*0.1f;
            //cout << r1 << " " << r2 << " " <<r3 << endl;
            vec3 col = vec3(.40+r1, .04+r2, .06 + r3);
            if((i+1) % 2 == 0){
                if(j == 0){
                    makeBrick(x,x+offset, y, y+brickHeight, 0, brickDepth, col, indice);
                }
                else{
                    makeBrick(x-offset,x+brickWidth-offset, y, y+brickHeight, 0, brickDepth, col, indice);
                }
                indice += 24;
                if(j < bricksWide){
                    makeBrick(x+brickWidth-offset,x+brickWidth+brickSpacing-offset, y, y+brickHeight, .01f, brickDepth-.01, vec3(0.5,0.5,0.5), indice);
                    indice += 24;
                }
                if(j == bricksWide-1){
                    makeBrick(x+brickWidth+brickSpacing-offset,x+brickWidth, y, y+brickHeight, 0, brickDepth, col, indice);
                    indice += 24;
                }
            }
            else{
                makeBrick(x,x+brickWidth, y, y+brickHeight, 0, brickDepth, col, indice);
                indice += 24;
                if(j < bricksWide-1){
                    makeBrick(x+brickWidth,x+brickWidth+brickSpacing, y, y+brickHeight, .01f, brickDepth-.01, vec3(0.5,0.5,0.5), indice);
                    indice += 24;
                }
            }
            x += (brickWidth+brickSpacing);
        }
        if(i < bricksTall - 1){
            makeBrick(minX, maxX, y+brickHeight, y+brickHeight+brickSpacing, .01f, brickDepth-.01, vec3(0.5,0.5,0.5), indice);
            indice += 24;
        }
        x = minX;
        y += (brickHeight+brickSpacing);
    }
}

void GLWidget::renderWall() {
    glUseProgram(wallProg);
    glBindVertexArray(wallVao);
    glDrawElements(GL_TRIANGLE_FAN, indices.size(), GL_UNSIGNED_INT, 0);
}

void GLWidget::initializeSmoothModel(const tinyobj::shape_t &shape,
                                     vector<vec3> &positions,
                                     vector<vec3> &normals,
                                     vector<unsigned int> &indices) {
    //positions.clear();
    //normals.clear();
    size_t posSize = positions.size();
    int normSize = normals.size();
    size_t indSize = indices.size();

    // Part 2 - The following code is just a copy of initializeFlatModel
    // with the exception of the indices array. We can save space in our
    // position buffer by constructing our primitives using unsigned
    // integers that reference specific indices in our position buffer.
    // The provided code still duplicates the positions of our vertices,
    // but uses an indices array to specify which vertices form triangles.
    // It mimicks the behavior of glDrawArrays (but is using glDrawElements),
    // by creating an indices array that goes from 0 to 3*(number of triangles)-1.
    // If you copy over your normal calculation from Part 1, you'll see the model
    // is still flat shaded because we haven't smoothed the normals yet.
    // You'll do that below.

    // Part 2a - First, optimize the code below by only adding each vertex position a single
    // time, rather than once per triangle it's used in. This is essentially just copying
    // the positions from shape.mesh.positions into positions.

    // Part 2b - Initialize normals to the same size as positions and set each value to
    // (0,0,0) before iterating over each triangle.

    // Part 2c - For every triangle in the model, calculate it's normal and add it to the
    // normal that corresponds to each of the triangle's vertex positions.

    // Part 2d - Populate your indices array with the index data of your mesh. This involves
    // copying over the index array from shape.mesh.indices to your own indices array.

    // Part 2e - Loop over every vertex normal and normalize it. Your normals are now
    // smoothed.

    for(size_t pos = 0; pos < shape.mesh.positions.size()/3; pos++){
        // Using those indices we can get the three points of the triangle
        vec3 p0 = vec3(shape.mesh.positions[3*pos+0],
                       shape.mesh.positions[3*pos+1],
                       shape.mesh.positions[3*pos+2]);

        // Part 1 - TODO Calculate the normal of the triangle
        vec3 n = vec3(0,0,0);

        // Push the points onto our position array
        positions.push_back(p0);

        // Push the normal onto our normal array (once for each point)
        normals.push_back(n);
    }

    for(size_t tri = 0; tri < shape.mesh.indices.size() / 3; tri++) {
        // Here we're getting the indices for the current triangle
        unsigned int ind0 = shape.mesh.indices[3*tri+0];
        unsigned int ind1 = shape.mesh.indices[3*tri+1];
        unsigned int ind2 = shape.mesh.indices[3*tri+2];

        indices.push_back(ind0+posSize);
        indices.push_back(ind1+posSize);
        indices.push_back(ind2+posSize);
    }
    for(size_t i = indSize; i < indices.size()-2; i+=3){
        vec3 v1 = positions[indices[i+1]] - positions[indices[i]];
        vec3 v2 = positions[indices[i+2]] - positions[indices[i]];
        v1 = normalize(cross(v1,v2));
        normals[indices[i]] += v1;
        normals[indices[i+1]] += v1;
        normals[indices[i+2]] += v1;
    }
    for(int j = normSize; j < normals.size(); j++){
        normals[j] = normalize(normals[j]);
    }
}

// Initialize all the necessary OpenGL state to be able
// to render a model
void GLWidget::initializeModel(const char* filename) {
    // shapes and materials are required std::vectors that will be populated
    // by tinyobj::LoadObj, see ../include/tinyobjloader for the code
    // Look at ../include/tinyobjloader/tiny_obj_loader.h
    // for the data structures that make up shape_t, mesh_t and material_t
    // Summary:
    //  A shape consists of a mesh and a name
    //  A mesh is what we're mostly concerned with and contains arrays
    //  for positions, normals, texture coordinates, and indices.
    //  Normals and texture coordinates only exists if they're stored
    //  in the obj file. The models I've provided don't include normals
    //  and texture coordinates. You will be calculating the normals.
    vector<tinyobj::shape_t> shapes;
    vector<tinyobj::material_t> materials;

    QFile file(filename);
    string err = tinyobj::LoadObj(shapes, materials, file);

    if(!err.empty()) {
        cerr << err << endl;
        exit(1);
    }

    vector<vec3> smooth_positions;
    vector<vec3> smooth_normals;
    vector<unsigned int> smooth_indices;

// Use the first mesh in the obj file and to initialize positions, normals and indices
// for smooth shading
    int cur = 0;

    for(int j = 0; j < 5; j++){

        initializeSmoothModel(shapes[0], smooth_positions, smooth_normals, smooth_indices);

        for(int i = cur; i < smooth_positions.size(); i++){
            smooth_positions.at(i) *= vec3(.02f,.02f,-.02f);
            smooth_positions.at(i) += vec3(-3,-.22f,-2-(2*j));
        }
        //smooth_indices.push_back(GL_UNSIGNED_INT);
        cur = smooth_positions.size();

        initializeSmoothModel(shapes[0], smooth_positions, smooth_normals, smooth_indices);

        for(int i = cur; i < smooth_positions.size(); i++){
            smooth_positions.at(i) *= vec3(-.02f,.02f,-.02f);
            smooth_positions.at(i) += vec3(-6,-.22f,-2-(2*j));
        }
        cur = smooth_positions.size();
    }

    numSmoothIndices = smooth_indices.size();

    GLuint smoothPositionBuffer;
    glGenBuffers(1, &smoothPositionBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, smoothPositionBuffer);
    glBufferData(GL_ARRAY_BUFFER, smooth_positions.size()*sizeof(vec3), &smooth_positions[0], GL_STATIC_DRAW);

    GLuint smoothNormalBuffer;
    glGenBuffers(1, &smoothNormalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, smoothNormalBuffer);
    glBufferData(GL_ARRAY_BUFFER, smooth_normals.size()*sizeof(vec3), &smooth_normals[0], GL_STATIC_DRAW);

    GLuint smoothIndexBuffer;
    glGenBuffers(1, &smoothIndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, smoothIndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, smooth_indices.size()*sizeof(unsigned int), &smooth_indices[0], GL_STATIC_DRAW);

    // load our shaders
    GLuint program = loadShaders(":/model_vert.glsl", ":/model_frag.glsl");
    glUseProgram(program);
    modelProg = program;

    // get some variable positions
    GLint positionIndex = glGetAttribLocation(program, "position");
    GLint normalIndex = glGetAttribLocation(program, "normal");

    glGenVertexArrays(1, &modelSmoothVao);
    glBindVertexArray(modelSmoothVao);

    glBindBuffer(GL_ARRAY_BUFFER, smoothPositionBuffer);
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, smoothNormalBuffer);
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, smoothIndexBuffer);

    modelProjMatrixLoc = glGetUniformLocation(program, "projection");
    modelViewMatrixLoc = glGetUniformLocation(program, "view");
    modelModelMatrixLoc = glGetUniformLocation(program, "model");

    modelLightPosLoc = glGetUniformLocation(program, "lightPos");
    modelLightColorLoc = glGetUniformLocation(program, "lightColor");
    modelLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    glUniform3f(modelLightPosLoc, -5,15,-5);
    glUniform3f(modelLightColorLoc, 1,1,1);
    glUniform1f(modelLightIntensityLoc, 1);

    modelDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    modelAmbientColorLoc = glGetUniformLocation(program, "ambientColor");
    modelSpecularColorLoc = glGetUniformLocation(program, "specularColor");

    glUniform3f(modelAmbientColorLoc, .6, .4, .0);
    glUniform3f(modelDiffuseColorLoc, 1, .8, 0);
    glUniform3f(modelSpecularColorLoc, 1, 1, 0);
}

void GLWidget::initializeWall(){
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &wallVao);
    glBindVertexArray(wallVao);

    // Create a buffer on the GPU for position data
    glGenBuffers(1, &positionBuffer);
    glGenBuffers(1, &normalBuffer);
    glGenBuffers(1, &colorBuffer);
    glGenBuffers(1, &indexBuffer);


    // Load our vertex and fragment shaders into a program object
    // on the GPU
    GLuint program = loadShaders(":/vert.glsl", ":/frag.glsl");
    glUseProgram(program);
    wallProg = program;

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    GLint normalIndex = glGetAttribLocation(program, "normal");
    glEnableVertexAttribArray(normalIndex);
    glVertexAttribPointer(normalIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

    makeWall(14, 12, .7f, .3f, .6f, .07f);
    for(int i = 0; i < pts.size(); i++){
        vec3 temp = pts[i];
        pts[i] = vec3(temp.z,temp.y,temp.x);
        pts[i] += vec3(-1.7f, -.5f, -5.05f);
    }
    int cur = pts.size();

    makeWall(12, 12, .7f, .3f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        vec3 temp = pts[i];
        pts[i] = vec3(temp.z,temp.y,temp.x);
        pts[i] += vec3(1.1f, -.5f, -4);
    }
    cur = pts.size();

    makeWall(11, 10, .5f, .4f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        pts[i] += vec3(1.4f, -.5f, -11.f);
    }
    cur = pts.size();

    makeWall(8, 12, .6f, .3f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        pts[i] += vec3(4.34f, -.5f, -0.02f);
    }
    cur = pts.size();

    makeWall(18, 12, .7f, .3f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        vec3 temp = pts[i];
        pts[i] = vec3(temp.z,temp.y,temp.x);
        pts[i] += vec3(6.95f, -.5f, -6.77f);
    }
    cur = pts.size();

    makeWall(11, 12, .7f, .3f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        vec3 temp = pts[i];
        pts[i] = vec3(temp.z,temp.y,temp.x);
        pts[i] += vec3(3.9f, -.5f, -6.2f);
    }
    cur = pts.size();

    makeWall(8, 12, .6f, .3f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        pts[i] += vec3(-4.34f, -.5f, -0.295f);
    }
    cur = pts.size();

    makeWall(16, 12, .875f, .3f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        pts[i] += vec3(0.f, -.5f, -14.25f);
    }
    cur = pts.size();

    makeWall(14, 12, .9315f, .3f, .6f, .07f);
    for(int i = cur; i < pts.size(); i++){
        vec3 temp = pts[i];
        pts[i] = vec3(temp.z,temp.y,temp.x);
        pts[i] += vec3(-7.584f, -.5f, -6.67f);
    }
    cur = pts.size();

    makeWall(6, 10, .4f, .4f, .4f, .04f);
    for(int i = cur; i < pts.size(); i++){
        pts[i] += vec3(5.8f, -.5f, -10.8f);
    }
    moveWallStart = cur;
    cur = pts.size();


    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, pts.size()*sizeof(vec3), &pts[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, normalBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, normals.size()*sizeof(vec3), &normals[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, colors.size()*sizeof(vec3), &colors[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(int), &indices[0], GL_DYNAMIC_DRAW);



    wallProjMatrixLoc = glGetUniformLocation(program, "projection");
    wallViewMatrixLoc = glGetUniformLocation(program, "view");
    wallModelMatrixLoc = glGetUniformLocation(program, "model");

    wallLightPosLoc = glGetUniformLocation(program, "lightPos");
    wallLightPosLoc2 = glGetUniformLocation(program, "lightPos2");
    wallLightColorLoc = glGetUniformLocation(program, "lightColor");
    wallLightIntensityLoc = glGetUniformLocation(program, "lightIntensity");

    glUniform3f(wallLightPosLoc, 5,10,-2);
    glUniform3f(wallLightPosLoc2, -5,10,-5);
    glUniform3f(wallLightColorLoc, 1,1,1);
    glUniform1f(wallLightIntensityLoc, 1);

    wallDiffuseColorLoc = glGetUniformLocation(program, "diffuseColor");
    wallAmbientColorLoc = glGetUniformLocation(program, "ambientColor");
    wallSpecularColorLoc = glGetUniformLocation(program, "specularColor");

    glUniform3f(wallAmbientColorLoc, .5, .1, .1);
    glUniform3f(wallDiffuseColorLoc, .4, 0, 0);
    glUniform3f(wallSpecularColorLoc, .2, .2, .2);

    update();
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    glEnable(GL_DEPTH_TEST);
    GLuint restart = 0xFFFFFFFF;
    glPrimitiveRestartIndex(restart);
    glEnable(GL_PRIMITIVE_RESTART);

    initializeWall();
    initializeGrid();
    initializeModel(":/models/bunny.obj");

    viewMatrix = mat4(1.0f);
    modelMatrix = mat4(1.0f);

    glUseProgram(wallProg);
    glUniformMatrix4fv(wallViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(wallModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(modelModelMatrixLoc, 1, false, value_ptr(modelMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUniformMatrix4fv(gridModelMatrixLoc, 1, false, value_ptr(modelMatrix));
}

void GLWidget::resizeGL(int w, int h) {
    width = w;
    height = h;

    float aspect = (float)w/h;

    projMatrix = perspective(45.0f, aspect, .01f, 100.0f);

    glUseProgram(wallProg);
    glUniformMatrix4fv(wallProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(modelProg);
    glUniformMatrix4fv(modelProjMatrixLoc, 1, false, value_ptr(projMatrix));

    glUseProgram(gridProg);
    glUniformMatrix4fv(gridProjMatrixLoc, 1, false, value_ptr(projMatrix));
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderGrid();
    renderWall();
    renderModelSmooth();
}

void GLWidget::renderModelSmooth() {
    glUseProgram(modelProg);
    glBindVertexArray(modelSmoothVao);
    glDrawElements(GL_TRIANGLES, numSmoothIndices, GL_UNSIGNED_INT, 0);
}

void GLWidget::renderGrid() {
    glUseProgram(gridProg);
    glBindVertexArray(gridVao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    {
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cout << "Shader linker failed: " << log << std::endl;
            delete [] log;
        }
    }

    return program;
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            forward = true;
            break;
        case Qt::Key_A:
            left = true;
            break;
        case Qt::Key_S:
            back = true;
            break;
        case Qt::Key_D:
            right = true;
            break;
        case Qt::Key_Tab:
            fly = !fly;
            break;
        case Qt::Key_Shift:
            down = true;
            break;
        case Qt::Key_Space:
            up = true;
            break;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_W:
            forward = false;
            break;
        case Qt::Key_A:
            left = false;
            break;
        case Qt::Key_S:
            back = false;
            break;
        case Qt::Key_D:
            right = false;
            break;
        case Qt::Key_Tab:
            // toggle fly mode
            break;
        case Qt::Key_Shift:
            down = false;
            break;
        case Qt::Key_Space:
            up = false;
            break;
    }
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    lastPt = pt;
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    vec2 pt(event->x(), event->y());
    vec2 d = pt-lastPt;
    //std::cout << d.x << " " << d.y << std::endl;

    // Part 1 - use d.x and d.y to modify your pitch and yaw angles
    // before constructing pitch and yaw rotation matrices with them
    yaw -= d.x/200;
    pitch -= d.y/150;

    if(pitch > M_PI/2)
        pitch = (float)M_PI/2;
    else if(pitch < -M_PI/2)
        pitch = (float)-M_PI/2;
    vec3 forwardVec = normalize(vec3(yawMatrix[0]));
    yawMatrix = glm::rotate(mat4(1.0), yaw, vec3(0,1,0));
    pitchMatrix = glm::rotate(mat4(1.0), pitch, forwardVec);
    viewMatrix = inverse(glm::translate(mat4(1.0f), position)*(pitchMatrix * yawMatrix));
    glUseProgram(wallProg);
    glUniformMatrix4fv(wallViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUseProgram(modelProg);
    glUniformMatrix4fv(modelViewMatrixLoc, 1, false, value_ptr(viewMatrix));
    glUseProgram(gridProg);
    glUniformMatrix4fv(gridViewMatrixLoc, 1, false, value_ptr(viewMatrix));

    lastPt = pt;
}
