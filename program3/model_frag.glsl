#version 330

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightIntensity;

uniform vec3 ambientColor;
uniform vec3 diffuseColor;
uniform vec3 specularColor;

uniform mat4 view;

in vec3 pos;
in vec3 norm;

out vec4 color_out;

void main() {
    vec3 L;
    vec3 N = normalize(norm);
    vec3 V = normalize((inverse(view))[3].xyz);
    L = normalize(lightPos-pos);

    vec3 diffuse = diffuseColor*dot(N,L);
    vec3 R = dot(L,N)*N;
    int shine = 3;
    float s = max(0, dot(V,R));
    s = pow(s,shine);
    vec3 specular = specularColor*s;

    color_out = vec4(lightIntensity*lightColor*(diffuse+specular)+ambientColor, 1);
}
