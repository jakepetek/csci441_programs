#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <tinyobjloader/tiny_obj_loader.h>
#include <QTimer>
#include <glm/glm.hpp>

#define GLM_FORCE_RADIANS

using glm::mat3;
using glm::mat4;
using glm::vec3;
using namespace std;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core { 
    Q_OBJECT

    public:
        GLWidget(QWidget *parent=0);
        ~GLWidget();

        GLuint loadShaders(const char* vertf, const char* fragf);
    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void keyReleaseEvent(QKeyEvent *event);

    public slots:
    // Part 2 - add an animate slot
        void animate();

    private:
        void initializeCube();
        void renderCube();

        GLuint cubeProg;
        GLuint cubeVao;
        GLint cubeProjMatrixLoc;
        GLint cubeViewMatrixLoc;
        GLint cubeModelMatrixLoc;
        GLuint textureObject;

        void initializeGrid();
        void renderGrid();

        GLuint gridProg;
        GLuint gridVao;
        GLint gridProjMatrixLoc;
        GLint gridViewMatrixLoc;
        GLint gridModelMatrixLoc;

        mat4 projMatrix;
        mat4 viewMatrix;
        mat4 modelMatrix;

        // Part 1 - Add two mat4 variables for pitch and yaw.
        // Also add two float variables for the pitch and yaw angles.
        float yaw;
        float pitch;
        mat4 yawMatrix;
        mat4 pitchMatrix;
        // Part 2 - Add a QTimer variable for our render loop.
        QTimer *timer;
        // Part 3 - Add state variables for keeping track
        //          of which movement keys are being pressed
        //        - Add two vec3 variables for position and velocity.
        //        - Add a variable for toggling fly mode
        bool forward;
        bool back;
        bool right;
        bool left;
        bool fly;
        bool up;
        bool down;

        vec3 position;
        vec3 velocity;

        int width;
        int height;

        glm::vec2 lastPt;
        void updateView();

        GLuint positionBuffer;
        GLuint colorBuffer;
        GLuint indexBuffer;
        GLuint normalBuffer;

        vector<vec3> pts;
        vector<vec3> colors;
        vector<int> indices;
        vector<vec3> normals;

        void initializeWall();
        void makeBrick(float x1, float x2, float y1, float y2, float z1, float z2, vec3 color, int indice);
        void makeWall(int bricksWide, int bricksTall, float brickWidth, float brickHeight, float brickDepth, float brickSpacing);
        void renderWall();

        GLuint wallProg;
        GLuint wallVao;
        GLint wallProjMatrixLoc;
        GLint wallViewMatrixLoc;
        GLint wallModelMatrixLoc;
        GLint wallRotateMatrixLoc;

        void initializeSmoothModel(const tinyobj::shape_t &shape,
                                   vector<vec3> &positions,
                                   vector<vec3> &normals,
                                   vector<unsigned int> &indices);
        void initializeModel(const char* filename);
        void renderModelSmooth();

        GLuint modelProg;
        GLuint modelSmoothVao;
        GLint modelProjMatrixLoc;
        GLint modelViewMatrixLoc;
        GLint modelModelMatrixLoc;

        GLint modelLightPosLoc;
        GLint modelLightColorLoc;
        GLint modelLightIntensityLoc;

        GLint modelDiffuseColorLoc;
        GLint modelAmbientColorLoc;
        GLint modelSpecularColorLoc;

        GLint wallDiffuseColorLoc;
        GLint wallAmbientColorLoc;
        GLint wallSpecularColorLoc;

        GLint gridDiffuseColorLoc;
        GLint gridAmbientColorLoc;
        GLint gridSpecularColorLoc;

        int numSmoothIndices;

        float time;
        int moveWallStart;

        GLint gridLightPosLoc;
        GLint gridLightColorLoc;
        GLint gridLightIntensityLoc;

        GLint wallLightPosLoc;
        GLint wallLightPosLoc2;
        GLint wallLightColorLoc;
        GLint wallLightIntensityLoc;

};

#endif
