#version 330

// Part 3 - Add a trackball matrix variable which is continuously updated
// as you click and drag. Multiply it into the chain of transformations
// below.

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

in vec3 position;
in vec3 normal;
in vec3 color;

out vec3 fposition;
out vec3 fnormal;
out vec3 fcolor;

void main() {
  gl_Position = projection * view * model * vec4(position, 1);
  fcolor = color;
  fnormal = (transpose(inverse(model))*vec4(normal,0)).xyz;
  fposition = (model*vec4(position,1)).xyz;
}
