#version 330

uniform mat4 projection;
in vec2 position;
in vec4 color;
out vec4 frag_color;

void main() {
  gl_Position = vec4(position.x, position.y, 0, 1);
  frag_color = color;
}
