## Jake Petek -- Program 1

# Controls
* C: Clears the screen of all points and colors
* UP: Increases the radius of the draw shape by 1 (Range:[1,50])
* DOWN: Decreases the radius of the draw shape by 1 (Range:[1,50])
* RIGHT: Increases the number of sides on the draw shape by 1 (Range:[3,20])
* LEFT: Decreases the number of sides on the draw shape by 1 (Range:[3,20])
* A: Decreases the fill densisty of any fill action by 0.05 (Range:[0,1])
* S: Increases the fill densisty of any fill action by 0.05 (Range:[0,1])
* F: Uniformly fills the screen using the current shape and fill density
* G: Randomly fills the screen using the current shape and fill density
* W: Changes the current color of the shift color option
* Q: Shifts all values of the current shift color by -0.05 (Range:[0,1])
* E: Shifts all values of the current shift color by 0.05 (Range:[0,1])
* Mouse Click: Draws the current shape centered at the mouse location
* Mouse Drag: Draws with the current shape as the mouse is dragged

# Features Explanation
* Clear: Simply clears all vertices from the points and color vectors.
* Radius: The shape radius determines how large a drawn shape will be. Can be 
between 1 and 50.
* Sides: The shape sides determines how many edges the regular polygon that is 
drawn will have. The number of sides can be between 3 and 20.
* Fill Density: The fill density changes the distance between shape when using 
a fill action.
* Uniform Fill: Uniformly fills the screen with rows of the current shape. Every 
other row is slightly offset to improve the aesthetic of the fill function.
* Random Fill: Randomly fills the screen with the current shape. The number of 
shapes is based on a ratio of the window size to the shape radius.
* Color Shift: Shifts the values of the selected color up or down.
* Available Draw Space: The valid draw space is determined based on the current 
aspect ratio of both the window and the image. If the aspect ratio of the image 
is larger than that of the window there will be space at the bottom of the window 
that cannot be drawn on. If the ascpect ration of the image is smaller than that 
of the window there will be space on the right side of the window that cannot be 
drawn on. If the two aspect ratios are equal, the entire window can be drawn on. 
This feature was added in order to preserve the aspect ratio of the image and 
prevent any distortions.
* Resizing: When the image is resized all of the previously drawn polygons are 
altered in the same way that the window was. This was done by using an 
orthographic projection matrix.
* Image Input: The project takes a PNG image as a command line argument in the 
form of an array of characters representing a file path. If no path is specified 
a default image is used instead. In this case the defualt image path is "..\hike.png".

# Image Selection
The image I chose was is of me and two of my roommates sitting on the trail up to 
the 'M' here in Bozeman. The picture was taken by our other roommate. I like this 
picture becuase of the great lighting provided by the evening sun, and especially 
becuase no one in the picture is acutally aware they are having their picture taken. 