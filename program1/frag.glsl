#version 330

out vec4 color_out;
in vec4 frag_color;

void main() {
  color_out = frag_color;
}
