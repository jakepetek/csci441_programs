#include "glwidget.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QTextStream>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using namespace std;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::value_ptr;
using glm::ortho;
using glm::mat4;

//Setting base image
GLWidget::GLWidget(char* input){
    //Sets the picture to input
    QString qInput = input;
    picture = QImage(qInput, "png");

    //If input is empty, sets the picture to a default location
    if(qInput.isNull()){
        picture = QImage(QString("..\\hike.png"), "png");
        cout<< "Set defualt image" << endl;
    }
    radius = 5;
    shape = 3;
    fillDensity = 0.5f;
    shiftColor = red;
    width = 960;
    height = 720;
    points.clear();
    colors.clear();
    numpts = 0;
}

void GLWidget::drawShape(vec2 p){
    vec2 img = windowToImage(p);
    if(img.x >= 0 && img.x < picture.width() && img.y >= 0 && img.y < picture.height()){
        float angle = 2*M_PI/shape;
        vec2 prev = vec2(p.x,p.y-radius);
        for(int i = 0; i < shape; i++){
            vec2 cur = vec2(p.x-radius*cos(angle*(i+1)+(M_PI/2)),p.y-radius*sin(angle*(i+1)+(M_PI/2)));
            points.push_back(windowToDevice(p));
            points.push_back(windowToDevice(prev));
            points.push_back(windowToDevice(cur));
            prev = cur;
        }
        vec4 t = imageColor(img);
        for(int i = 0; i < shape*3; i++){
            colors.push_back(imageColor(img));
        }
        numpts += shape * 3;
    }
}

vec4 GLWidget::imageColor(vec2 p){
    QColor color = picture.pixel(p.x,p.y);
    return vec4(color.red()/255.0f,color.green()/255.0f,color.blue()/255.0f,color.alpha()/255.0f);
}

void GLWidget::shiftColors(float f){
    for(int i = 0; i < colors.size(); i++){
        if(shiftColor == red){
            colors[i].r = colors[i].r+f;
        }
        else if(shiftColor == green){
            colors[i].g = colors[i].g+f;
        }
        else{
            colors[i].b = colors[i].b+f;
        }
    }
    if(shiftColor == red){
        cout << "Red color values shifted by " << f << endl;
    }
    else if(shiftColor == green){
        cout << "Green color values shifted by " << f << endl;
    }
    else{
        cout << "Blue color values shifted by " << f << endl;
    }
}

void GLWidget::keyPressEvent(QKeyEvent *event) {
    switch(event->key()) {
        case Qt::Key_C:
            points.clear();
            colors.clear();
            numpts = 0;
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
            cout<<"Points Cleared"<<endl;
        break;

        case Qt::Key_F:
        {
            int count = 0;
            for(float i = radius; i < width; i += radius*(1.5+(1-fillDensity))){
                for(float j = radius; j < height; j += radius*(1.5+(1-fillDensity))){
                    if(count%2 == 1)
                        drawShape(vec2(i-radius,j));
                    else
                        drawShape(vec2(i,j));
                    count++;
                }
            }
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * points.size(), &points[0], GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * colors.size(), &colors[0], GL_DYNAMIC_DRAW);
            update();
            cout << "Uniform fill with shape with " << shape << " sides, a radius of " << radius <<" and a fill density of " << fillDensity << "."<<endl;
        }
        break;

        case Qt::Key_G:
            for(int i = 0; i < (0.5+fillDensity)*(width/radius); i++){
                for(int j = 0; j < (0.5+fillDensity)*(height/radius); j++){
                    drawShape(vec2((rand()%width),(rand()%height)));
                }
            }
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * points.size(), &points[0], GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * colors.size(), &colors[0], GL_DYNAMIC_DRAW);
            update();
            cout << "Random fill with shape with " << shape << " sides, a radius of " << radius <<" and a fill density of " << fillDensity << "."<<endl;
        break;

        case Qt::Key_W:
            if(shiftColor == red){
                shiftColor = green;
                cout << "Color for shift is now set to green." << endl;
            }
            else if(shiftColor == green){
                shiftColor = blue;
                cout << "Color for shift is now set to blue." << endl;
            }
            else{
                shiftColor = red;
                cout << "Color for shift is now set to red." << endl;
            }
        break;

        case Qt::Key_E:
            shiftColors(0.05f);
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * points.size(), &points[0], GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * colors.size(), &colors[0], GL_DYNAMIC_DRAW);
            update();
        break;

        case Qt::Key_Q:
            shiftColors(-0.05f);
            glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * points.size(), &points[0], GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * colors.size(), &colors[0], GL_DYNAMIC_DRAW);
            update();
        break;

        case Qt::Key_Left:
            if(shape > 3){
                shape--;
            }
            cout << "Shape now has " << shape << " sides." <<endl;
        break;

        case Qt::Key_Right:
            if(shape < 20){
                shape++;
            }
            cout << "Shape now has " << shape << " sides." <<endl;
        break;

        case Qt::Key_Up:
            if(radius < 50){
                radius++;
            }
            cout << "Shape now has radius " << radius << "." <<endl;
        break;

        case Qt::Key_Down:
            if(radius > 1){
                radius--;
            }
            cout << "Shape now has radius " << radius << "." <<endl;
        break;

        case Qt::Key_A:
            if(fillDensity > .05){
                fillDensity -= .05f;
            }
            else{
                fillDensity = 0;
            }
            cout << "Fill density is now set to " << fillDensity << "." <<endl;
        break;

        case Qt::Key_S:
            if(fillDensity < 1){
                fillDensity += .05f;
            }
            cout << "Fill density is now set to " << fillDensity << "." <<endl;
        break;
    }
    update();
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    drawShape(vec2(event->x(), event->y()));
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * points.size(), &points[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * colors.size(), &colors[0], GL_DYNAMIC_DRAW);
    update();
    /*for(int i =0 ; i < colors.size(); i++){
        vec4 t = colors[i];
        cout <<i <<": "<< t.r << " " << t.g << " " << " " << t.b << " " <<t.a << endl;
    }*/
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    drawShape(vec2(event->x(), event->y()));
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * points.size(), &points[0], GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * colors.size(), &colors[0], GL_DYNAMIC_DRAW);
    update();
}

vec2 GLWidget::windowToImage(vec2 p){
    //if picture aspect ratio is greater than or equal to window aspect ratio
    if((float)picture.width()/(float)picture.height() >= (float)width/(float)height){
        p.x = picture.width() * (p.x/width);
        float maxY = width*((float)picture.height()/(float)picture.width());
        p.y = (picture.height() * (p.y/maxY));
        //cout <<"width " << width << " height " << height << " MaxX " << width << " MaxY " << maxY << endl;
    }
    else{
        p.y = (picture.height() * (p.y/height));
        float maxX = height *((float)picture.width()/(float)picture.height());
        p.x = picture.width() * (p.x/maxX);
        //cout <<"width " << width << " height " << height << " MaxX " << maxX << " MaxY " << height<< endl;
    }
    return p;
}

vec2 GLWidget::windowToDevice(vec2 p){
    p.x = (p.x*2/width)-1;
    p.y = 1-p.y*2/height;
    return p;
}

//GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent){}

GLWidget::~GLWidget(){
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(program);
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, numpts);
}

void GLWidget::initializeGL() {
    initializeOpenGLFunctions();

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glPointSize(4.0f);

    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Create a buffer on the GPU for position data
    glGenBuffers(1, &positionBuffer);
    glGenBuffers(2, &colorBuffer);

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);

    // Load our vertex and fragment shaders into a program object
    // on the GPU
    program = loadShaders(":/vert.glsl", ":/frag.glsl");

    // Bind the attribute "position" (defined in our vertex shader)
    // to the currently bound buffer object, which contains our
    // position data for a single triangle. This information
    // is stored in our vertex array object.
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    GLint positionIndex = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
    GLint colorIndex = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(colorIndex);
    glVertexAttribPointer(colorIndex, 4, GL_FLOAT, GL_FALSE, 0, 0);

    projectionIndex = glGetUniformLocation(program, "projection");
}

void GLWidget::resizeGL(int w, int h) {
    glViewport(0,0,w,h);
    width = w;
    height = h;

    mat4 projMatrix = glm::ortho(0.0f, (float)w, (float)h, 0.0f);
    glUseProgram(program);
    glUniformMatrix4fv(projectionIndex,1,GL_FALSE,value_ptr(projMatrix));
}

GLuint GLWidget::loadShaders(const char* vertf, const char* fragf) {
    GLuint program = glCreateProgram();

    // read vertex shader from Qt resource file
    QFile vertFile(vertf);
    vertFile.open(QFile::ReadOnly | QFile::Text);
    QString vertString;
    QTextStream vertStream(&vertFile);
    vertString.append(vertStream.readAll());
    std::string vertSTLString = vertString.toStdString();

    const GLchar* vertSource = vertSTLString.c_str();

    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &vertSource, NULL);
    glCompileShader(vertShader);
    {
        GLint compiled;
        glGetShaderiv( vertShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( vertShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( vertShader, len, &len, log );
            std::cout << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, vertShader);

    // read fragment shader from Qt resource file
    QFile fragFile(fragf);
    fragFile.open(QFile::ReadOnly | QFile::Text);
    QString fragString;
    QTextStream fragStream(&fragFile);
    fragString.append(fragStream.readAll());
    std::string fragSTLString = fragString.toStdString();

    const GLchar* fragSource = fragSTLString.c_str();

    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &fragSource, NULL);
    glCompileShader(fragShader);
    {
        GLint compiled;
        glGetShaderiv( fragShader, GL_COMPILE_STATUS, &compiled );
        if ( !compiled ) {
            GLsizei len;
            glGetShaderiv( fragShader, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetShaderInfoLog( fragShader, len, &len, log );
            std::cerr << "Shader compilation failed: " << log << std::endl;
            delete [] log;
        }
    }
    glAttachShader(program, fragShader);

    glLinkProgram(program);
    {
        GLint linked;
        glGetProgramiv( program, GL_LINK_STATUS, &linked );
        if ( !linked ) {
            GLsizei len;
            glGetProgramiv( program, GL_INFO_LOG_LENGTH, &len );

            GLchar* log = new GLchar[len+1];
            glGetProgramInfoLog( program, len, &len, log );
            std::cout << "Shader linker failed: " << log << std::endl;
            delete [] log;
        }
    }

    return program;
}
