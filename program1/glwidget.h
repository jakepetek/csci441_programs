#ifndef __GLWIDGET__INCLUDE__
#define __GLWIDGET__INCLUDE__

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QMouseEvent>
#include <glm/glm.hpp>
#include <vector>

// glm by default uses degrees, but that functionality
// is deprecated so GLM_FORCE_RADIANS turns off some
// glm warnings
#define GLM_FORCE_RADIANS

using namespace std;
using glm::vec2;
using glm::vec4;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core {
    Q_OBJECT

    public:
        //GLWidget(QWidget *parent=0);
        GLWidget(char* pLocation);
        ~GLWidget();

    protected:
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void keyPressEvent(QKeyEvent *event);
        void drawShape(vec2 p);
        void shiftColors(float f);
        vec4 imageColor(vec2 p);
        vec2 windowToImage(vec2 p);
        vec2 windowToDevice(vec2 p);

    private:
        GLuint loadShaders(const char* vertf, const char* fragf);

        GLuint vao;
        GLuint program;

        GLuint positionBuffer;
        GLuint colorBuffer;
        GLuint projectionIndex;
        QImage picture;
        int radius, shape, width, height, numpts;
        float fillDensity;
        enum Color {red, green, blue};
        Color shiftColor;
        vector<vec2> points;
        vector<vec4> colors;
};

#endif
